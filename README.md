# Proyecto Colegio

Este proyecto se generó con [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6.

## Ejecución

Ejecutar `ng serve` para iniciar el proyecto. Ingresa a la ruta `http://localhost:4200/`.

Usuario y contraseña asignada a la base de datos embebida es:

* Usuario: sa
* Contraseña: sa

## Herramientas Utilizadas

- Spring tool 4
- Java 8
- H2
- Angular 11
- Visual code

## Peticiones através de POSTMAN

En la raiz del del proyecto ColegioBackend, se encuentra el archivo POSTMAN para hacer la importación del mismo y evidenciar los Endpoint API.

`Colegio.postman_collection.json` 


## Autor
* Jeison Andrés Soler

