import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Asignatura } from '../model/Asignatura';
import { Estudiante } from '../model/Estudiante';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  private url : string = environment.urlBase + "/asignatura/profesor/";
  private urlEstudiantes : string = environment.urlBase + "/asignatura/#idEstudiante#/estudiantes/";

  constructor(private http : HttpClient) { }

  obtenerListaPorProfesor(id : string) {
    let newUrl = this.url + id;
    return this.http.get<Asignatura[]>(newUrl);
  }

  obtenerEstudiantesPorAsignatura(id : number) {
    let newUrl = this.urlEstudiantes.replace('#idEstudiante#', id.toString());
    return this.http.get<Estudiante[]>(newUrl);
  }
}
