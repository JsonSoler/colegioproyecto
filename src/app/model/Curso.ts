export interface Curso {
    id: number;
    grado: number;
    salon: string;
}