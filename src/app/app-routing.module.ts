import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsignaturaComponent } from './asignatura/asignatura.component';
import { ProfesorComponent } from './profesor/profesor.component';

const routes: Routes = [
  { path: '', redirectTo: '/profesores', pathMatch: 'full' },
  {path: 'profesores', component: ProfesorComponent},
  {path: 'profesor/:idProfesor/asignaturas', component: AsignaturaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
