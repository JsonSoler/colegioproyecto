import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Profesor } from '../model/Profesor';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  private url : string = environment.urlBase + "/profesores";

  constructor(private http : HttpClient) { }

  obtenerListaProfesores() {
    return this.http.get<Profesor[]>(this.url);
  }
}
