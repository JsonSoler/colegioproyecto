import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../model/Asignatura';
import { AsignaturaService } from '../services/asignatura.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-asignatura',
  templateUrl: './asignatura.component.html',
  styleUrls: ['./asignatura.component.css']
})
export class AsignaturaComponent implements OnInit {

  asignaturas: Asignatura[] = [];
  panelOpenState = false;

  constructor(
    private asignaturaService: AsignaturaService,
    private route : ActivatedRoute,
    private Location : Location
    ) { }

  ngOnInit(): void {
    this.cargarAsignaturas();
  }

  private cargarAsignaturas() {
    //let idProfesor = this.route.snapshot.paramMap.get('idProfesor');
    let idProfesor = this.route.snapshot.params.idProfesor;
    this.asignaturaService.obtenerListaPorProfesor(idProfesor).subscribe(response => {
      this.asignaturas = response;
    });
  }

  cargarEstudiantes(id : number) {
    //const asignaturaActual = this.asignaturas.find(asignatura => asignatura.id == id);
    //const indexArray = this.asignaturas.indexOf(asignaturaActual!);

    console.log('entrando en el evento opened');

    const indexArray = this.asignaturas.findIndex(asignatura => asignatura.id == id);

    console.log('index del arreglo-->'+indexArray);

    if(this.asignaturas[indexArray].estudiantes == null) {
      this.asignaturaService.obtenerEstudiantesPorAsignatura(id).subscribe(response => {
        this.asignaturas[indexArray].estudiantes = response;
      });
    }
  }
}
