import { Component, OnInit } from '@angular/core';
import { Profesor } from '../model/Profesor';
import { ProfesorService } from '../services/profesor.service';


@Component({
  selector: 'profesor-componente',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})
export class ProfesorComponent implements OnInit {

  profesores : Profesor[] = [];

  constructor(private profesorService :  ProfesorService) { }

  ngOnInit(): void {
    this.cargarLista();
       }

  private cargarLista() {
    this.profesorService.obtenerListaProfesores().subscribe(response => {
      this.profesores = response;
     
    });
  }

  obtenerLinkAsignatura(id : number) : string {
    let url = "/profesor/asignaturas/"+id;
    return url;
  }

}
