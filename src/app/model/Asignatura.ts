import { Curso } from "./Curso";
import { Estudiante } from "./Estudiante";

export interface Asignatura {
    id: number;
    nombre: string;
    curso: Curso;
    estudiantes: Estudiante[];
}